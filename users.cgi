#!/usr/bin/perl
use strict;
use warnings;
use File::Path qw(make_path);
use CGI qw(:all -utf8);
use CGI::Carp qw(fatalsToBrowser set_message);
use Encode;
use utf8;
binmode STDOUT, ':utf8';

# Директория с ключами
our $dir = '/home/git/users';

our $version = '0.01';

my $cgi = CGI->new;

goto $cgi->request_method;

GET:
my @authors = authors();
print $cgi->header(-charset    => 'utf-8'),
      $cgi->start_html( -title => 'Users',
                        -style => [{'src'        => 'css/style.css'},
                                   {'src'        => 'css/facebox.css'}],
                        -head  => Link({-rel => 'shortcut icon', -href => '/kogakure/git-favicon.png'}),
                        -meta  => {'description' => 'Users',
                                   'generator'   => "stl/$version",
                                   'robots'      => 'noindex,nofollow'},
                        -script=> [{-type => 'javascript',
                                    -src  => 'js/add-remove.js'},
                                   {-type => 'javascript',
                                    -src  => 'js/validate.js'},
                                   {-type => 'javascript',
                                    -src  => 'js/jquery.js'},
                                   {-type => 'javascript',
                                    -src  => 'js/facebox.js'},
                                   {-type => 'javascript',
                                   -code => "jQuery(document).ready(function(\$) {
      \$('a[rel*=facebox]').facebox({
        loadingImage : 'img/loading.gif',
        closeImage   : 'img/closelabel.png'
      })
    })"}]),
      # start div id='wrapper'
      $cgi->start_div({-id => 'wrapper'}),
        # div class='navbar'
        $cgi->div({-class => 'navbar'},
          # div id='navbar-inner'
          $cgi->div({-class => 'navbar-inner'},
            # start navigation menu
            # div class='brand'
            $cgi->div({-class  => 'brand'},
              $cgi->a({     -href   => '/',
                            -title  => 'BKS Git Server'},
                $cgi->img({ -src    => '/kogakure/git-logo.png',
                            -width  => 72,
                            -height => 27,
                            -alt    => 'git',
                            -class  => 'logo'}))),
            # end div class='brand'
            $cgi->ul({-class => 'nav'},
              $cgi->li($cgi->a({-href => '/'}, 'Home')),
              $cgi->li($cgi->a({-href => '/handler/create.cgi'}, 'Create repository')),
              $cgi->li($cgi->a({-href => '/handler/edit.cgi'}, 'Edit repositories')),
              $cgi->li({-class => 'active'}, $cgi->a({-href => $cgi->url(-absolute => 1)}, 'Users'))))),
            # end navigation menu
        # end div class='navbar'
        # div class='container'
        $cgi->start_div({-class => 'container'});
          

# Получаем переданное имя
my $user = $cgi->param('n');
# Проверяем не пустое ли и существует ли файл
if ($user ne '' and -f "$dir/$user.pub") {
  my $name = join ' ', map(ucfirst, split(/\./, $user));
  my @keys = read_keys($user);
  print $cgi->h2("$name keys");
  my $i = 0;
  print $cgi->start_form(-id       => 'keys',
                         -name     => 'keys',
                         -class    => 'newform',
                         -action   => $cgi->url(-absolute => 1),
                         -method   => 'POST');
  foreach my $key (@keys) {
    $i++;
    print $cgi->start_div({-class => 'input-append'}),
            $cgi->textarea({-value => $key,
                            -name  => "key_$i"}),
            $cgi->start_span({-class => 'add-on cursor',
                              -onclick => 'removeField(this)'}),
              $cgi->start_i({-class => 'icon-remove'}),
              $cgi->end_i,
            $cgi->end_span,
          $cgi->end_div;
  }
  my @n;
  for(my $y=1;$y<=$i;$y++){push @n,$y}
  my $value = join ',', @n;
    print $cgi->hidden({-name  => 'values',
                        -value => $value}),
          $cgi->input({-type    => 'button',
                       -class   => 'btn btn-large',
                       -name    => 'add',
                       -value   => 'Добавить поле',
                       -onclick => 'addArea()'}),
          $cgi->submit({-name  => 'submit',
                        -class => 'btn btn-large',
                        -value => 'Применить изменения'}),
        $cgi->end_form();
}
else {
    print $cgi->h2('Пользователи'),
          $cgi->start_ul();
    # Вывести на печать всех авторов
    foreach my $name (@authors) {
      print $cgi->li(
              $cgi->a({-href => "?n=".doturl($name)},$name));
    }
    print $cgi->end_ul(),

}

        # end div class='container'
  print $cgi->end_div,
        # div class='footer'
        $cgi->div({-class => 'footer container'},
          $cgi->a({-href  => '#register',
                   -rel   => 'facebox',
                   -class => 'btn btn-small'},
            $cgi->start_i({-class => 'icon-plus'}),
            $cgi->end_i,
              $cgi->span('Добавить пользователя'))),
      #end div id='wrapper'
      $cgi->end_div,
      $cgi->div({-id    => 'register',
                 -style => 'display:none;'},
        $cgi->h4('Создание пользователя'),
        $cgi->start_form({-id => 'user-create',
                    -name     => 'user-create',
                    # -onsubmit => 'return validateUserForm()',
                    -action   => '/handler/createuser.cgi',
                    -method   => 'POST'}),
          $cgi->table({-class => 'table'},
            $cgi->Tr(
              $cgi->td('Имя'),
              $cgi->td(
                $cgi->input({-id        => 'id_name',
                             -name      => 'name',
                             -type      => 'text',
                             -autofocus => 'autofocus',
                             -maxlength => 255}))),
            $cgi->Tr(
              $cgi->td('Ключ'),
              $cgi->td(
                $cgi->textarea({-id   => 'id_key',
                                -name => 'key',
                                -type => 'text'}))),
            $cgi->Tr(
              $cgi->th({-colspan => 2},
                $cgi->submit({-name  => 'submit',
                              -value => 'Создать пользователя',
                              -type  => 'submit',
                              -class => 'btn'})))),
          $cgi->end_form),
      $cgi->end_html;

exit 0; # Exit from GET block

POST:
# Получение ключей и внесение их в файл с ключами
my $keys = $cgi->param('values');
my @f = split/,/, $keys;
my $file_name = $cgi->referer;
$file_name =~ s/.*\?n=(.*)/$1/;
open my($fh), q{>}, "/home/git/users/$file_name.pub";
print $file_name;
foreach my $key (@f) {
  print $fh $cgi->param("key_$key"),"\n" if $cgi->param("key_$key") ne '';
}
close $fh;
goto GET;

exit 0; # Exit from POST block

sub authors
{
  my @authors;
  my @files = <$dir/*.pub>;
  foreach my $file (@files) {
    $file =~ s/$dir\/(.*)\.pub/$1/;
    my $name = join ' ', map(ucfirst, split(/\./, $file));
    push @authors, $name;
  }
  return @authors;
}

sub doturl
{
  ($_) = @_;
  s# #.#;
  return lc $_;
}

sub read_keys
{
  my $user = shift;
  my @k;
  open my($fh), "$dir/$user.pub";
  while (<$fh>) {
    chomp;
    push @k, $_;
  }
  close $fh;
  return @k;
}