#!/usr/bin/perl
use strict;
use warnings;
use CGI qw(:all -utf8);
use CGI::Carp qw(fatalsToBrowser set_message);
use Encode;
use utf8;
binmode STDOUT, ':utf8';

# Директория с ключами
our $dir = '/home/git/users';

our $version = '0.01';

my $cgi = CGI->new;

goto $cgi->request_method;

GET:
print $cgi->header(-charset    => 'utf-8'),
    $cgi->start_html( -title => 'Create user',
            -style => {'src'         => 'css/style.css'},
            -head  => Link({-rel => 'shortcut icon', -href => '/kogakure/git-favicon.png'}),
            -meta  => {'description' => 'Users',
                       'generator'   => "stl/$version",
                       'robots'      => 'noindex,nofollow'},
            -script=> {-type => 'javascript',
                       -code => 'window.location.href="/handler/users.cgi";'}),
    $cgi->end_html;
exit 0; # Exit from GET block

POST:
my $user_name = lc(translit(clearspace($cgi->param('name'))));
my $user_key  = $cgi->param('key');

# Создание файла пользователя и внесение в него ключа
open my($fh), q{>>}, $dir.'/'.$user_name.'.pub' or die $!;
print $fh $user_key;
close $fh;

print $cgi->header(-charset    => 'utf-8'),
    $cgi->start_html( -title => 'Create user',
            -style => {'src'         => 'css/style.css'},
            -head  => Link({-rel => 'shortcut icon', -href => '/kogakure/git-favicon.png'}),
            -meta  => {'description' => 'Users',
                       'generator'   => "stl/$version",
                       'robots'      => 'noindex,nofollow'},
            -script=> {-type => 'javascript',
                       -code => 'window.location.href="/handler/users.cgi";'}),
    $cgi->end_html;

exit 0; # Exit from POST block      

sub clearspace
{
  ($_) = @_;
  s/ /\./;
  return $_;
}

sub translit
{
  ($_)=@_;

  s/Сх/S\'h/; s/сх/s\'h/; s/СХ/S\'H/;
  s/Ш/Sh/g; s/ш/sh/g;
  s/Сцх/Sc\'h/; s/сцх/sc\'h/; s/СЦХ/SC\'H/;
  s/Щ/Sch/g; s/щ/sch/g;
  s/Цх/C\'h/; s/цх/c\'h/; s/ЦХ/C\'H/;
  s/Ч/Ch/g; s/ч/ch/g;
  s/Йа/J\'a/; s/йа/j\'a/; s/ЙА/J\'A/;
  s/Я/Ja/g; s/я/ja/g;
  s/Йо/J\'o/; s/йо/j\'o/; s/ЙО/J\'O/;
  s/Ё/Jo/g; s/ё/jo/g;
  s/Йу/J\'u/; s/йу/j\'u/; s/ЙУ/J\'U/;
  s/Ю/Ju/g; s/ю/ju/g;
  s/Э/E\'/g; s/э/e\'/g;
  s/Е/E/g; s/е/e/g;
  s/Зх/Z\'h/g; s/зх/z\'h/g; s/ЗХ/Z\'H/g;
  s/Ж/Zh/g; s/ж/zh/g;
  tr/
  абвгдзийклмнопрстуфхцъыьАБВГДЗИЙКЛМНОПРСТУФХЦЪЫЬ/
  abvgdzijklmnoprstufhc\"y\'ABVGDZIJKLMNOPRSTUFHC\"Y\'/;
  
  return $_;
}
