#!/usr/bin/perl
use strict;
use warnings;
use File::Path qw(make_path);
use CGI qw(:all -utf8);
use CGI::Carp qw(fatalsToBrowser set_message);
use Encode;
use utf8;
binmode STDOUT, ':utf8';

# Директория с ключами
our $dir = '/home/git/users';

our $version = '0.01';

my $cgi = CGI->new;

goto $cgi->request_method;

GET:
my @authors = authors();
print $cgi->header(-charset    => 'utf-8'),
      $cgi->start_html( -title => 'Create repository',
                        -style => {'src'         => 'style.css'},
                        -head  => Link({-rel => 'shortcut icon', -href => '/kogakure/git-favicon.png'}),
                        -meta  => {'description' => 'Create git repository',
                                   'generator'   => "stl/$version",
                                   'robots'      => 'noindex,nofollow'}),
      # start div id='wrapper'
      $cgi->start_div({-id => 'wrapper'}),
        # div class='navbar'
        $cgi->div({-class => 'navbar'},
          # div id='navbar-inner'
          $cgi->div({-class => 'navbar-inner'},
            # start navigation menu
            # div class='brand'
            $cgi->div({-class  => 'brand'},
              $cgi->a({     -href   => '/',
                            -title  => 'BKS Git Server'},
                $cgi->img({ -src    => '/kogakure/git-logo.png',
                            -width  => 72,
                            -height => 27,
                            -alt    => 'git',
                            -class  => 'logo'}))),
            # end div class='brand'
            $cgi->ul({-class => 'nav'},
              $cgi->li($cgi->a({-href => '/'}, 'Home')),
              $cgi->li($cgi->a({-href => '/handler/create.cgi'}, 'Create repository')),
              $cgi->li($cgi->a({-href => '/handler/edit.cgi'}, 'Edit repositories')),
              $cgi->li({-class => 'active'}, $cgi->a({-href => $cgi->self_url}, 'Users'))))),
            # end navigation menu
        # end div class='navbar'
        # div class='container'
        $cgi->start_div({-class => 'container'}),
          $cgi->h2('Пользователи'),
          $cgi->start_ul();
    # Вывести на печать всех авторов
    foreach my $name (@authors) {
      print $cgi->li(
              $cgi->a({-href => "?n=".clearurl($name)},$name));
    }
    print $cgi->end_ul(),
        # end div class='container'
        $cgi->end_div,
        # div class='footer'
        $cgi->div({-class => 'footer'}),
      #end div id='wrapper'
      $cgi->end_div,
      $cgi->end_html;

exit 0; # Exit from GET block

POST:
print $cgi->header, "Don't work now.";

exit 0; # Exit from POST block

sub authors
{
  my @authors;
  my @files = <$dir/*.pub>;
  foreach my $file (@files) {
    $file =~ s/$dir\/(.*)\.pub/$1/;
    my $name = join ' ', map(ucfirst, split(/\./, $file));
    push @authors, $name;
  }
  return @authors;
}

sub clearurl
{
  ($_) = @_;
  s# #.#;
  return lc $_;
}
