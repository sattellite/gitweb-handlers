#!/usr/bin/perl
use strict;
use warnings;
use CGI;
use CGI::Carp qw(fatalsToBrowser set_message);
use Encode;
binmode STDOUT, ':utf8';