function addArea() {
  // Создаем элемент div и назначем имя класса
  var div = document.createElement('div');
  div.className = 'input-append';
  // Создаем элемент span, назначем класс и вешаем на клик эвент
  var span = document.createElement('span');
  span.className = 'add-on cursor';
  span.setAttribute('onClick', 'removeField(this)');
  // Создаем элемент i и назначаем класс
  var icon = document.createElement('i');
  icon.className = 'icon-remove';
  // Создаем элемент textarea и назначем имя
  var area = document.createElement('textarea');
  var key = keyNum()+1;
  area.setAttribute('name', 'key_'+key);
  // Собираем все элементы в один блок:
  // icon помещаем внутрь span, а затем span и testarea в нужном порядке
  // помещаем внутрь div.
  span.appendChild(icon);
  div.appendChild(area);
  div.appendChild(span);
  // Находим нужного родителя и помещаем получившийся блок
  // перед элементом с именем values
  var values = document.getElementsByName('values')[0];
  values.parentNode.insertBefore(div, values);
  // Обновление значения в скрытом поле
  updateHidden();
}
function keyNum() {
  // Получаем список всех полей textarea
  var allFields = document.getElementsByTagName('textarea');
  if (allFields.item(allFields.length - 1)) {
      // Выбираем последний, берем его имя, разбираем имя на две части и выбираем число,
      // переводим из строкового значения в числовое и отправляем.
      return parseInt(allFields.item(allFields.length-1).name.split('_')[1]);
  }
  else { return parseInt(0)}
}
function updateHidden() {
  // Получаем список всех полей textarea
  var allFields = document.getElementsByTagName('textarea');
  // Инициализируем массив
  var keys = [];
  // Берем значение ключа каждого найденного элемента и добавляем в массив
  for (var i = 0; i < allFields.length; i++) {
      keys.push(allFields.item(i).name.split('_')[1]);
  };
  // Находим скрытое поле
  var elem = document.getElementsByName('values')[0];
  // И выставляем все номера ключей в значение
  elem.setAttribute('value', keys.join());
}
function removeField(elem) {
    // Получаем элемент, вычисляем его родителя. Потом вычисляем родителя этого родителя
    // и говорим последнему родителю удалить своего ребенка. В качестве ребенка указываем
    // родителя полученного элемента.
    elem.parentNode.parentNode.removeChild(elem.parentNode);
    // Обновление значения в скрытом поле
    updateHidden();
}