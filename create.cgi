#!/usr/bin/perl
use strict;
use warnings;
use CGI qw(:all -utf8);
use CGI::Carp qw(fatalsToBrowser set_message);
use Encode;
use utf8;
binmode STDOUT, ':utf8';

our $version = '0.2';
our $repos_dir = '/projects';
our $keys_dir  = '/home/git/users';

my $cgi = CGI->new;

goto $cgi->request_method;

GET:
my @id_repo_values = repo_values();
my %id_repo_labels = repo_labels();
my @id_author_values = author_values();
my %id_author_labels = author_labels();

print $cgi->header(-charset    => 'utf-8'),
      $cgi->start_html( -title => 'Create repository',
                        -style => {'src'         => 'css/style.css'},
                        -meta  => {'description' => 'Create git repository',
                                   'generator'   => "stl/$version",
                                   'robots'      => 'noindex,nofollow'},
                        -head  => Link({-rel => 'shortcut icon', -href => '/kogakure/git-favicon.png'}),
                        -script=> {-type => 'javascript',
                                   -src  => 'js/validate.js'}),
      # start div id='wrapper'
      $cgi->start_div({-id => 'wrapper'}),
        # div class='navbar'
        $cgi->div({-class => 'navbar'},
          # div id='navbar-inner'
          $cgi->div({-class => 'navbar-inner'},
            # start navigation menu
            # div class='brand'
            $cgi->div({-class  => 'brand'},
              $cgi->a({     -href   => '/',
                            -title  => 'BKS Git Server'},
                $cgi->img({ -src    => '/kogakure/git-logo.png',
                            -width  => 72,
                            -height => 27,
                            -alt    => 'git',
                            -class  => 'logo'}))),
            # end div class='brand'
            $cgi->ul({-class => 'nav'},
              $cgi->li($cgi->a({-href => '/'}, 'Home')),
              $cgi->li({-class => 'active'}, $cgi->a({-href => $cgi->self_url}, 'Create repository')),
              $cgi->li($cgi->a({-href => '/handler/edit.cgi'}, 'Edit repositories')),
              $cgi->li($cgi->a({-href => '/handler/users.cgi'}, 'Users'))))),
            # end navigation menu
        # end div class='navbar'
        # div class='container'
        $cgi->start_div({-class => 'container'}),
          $cgi->h2('Создание репозитория'),
          # start form
          $cgi->start_form(-id       => 'repo-create',
                           -name     => 'repo-create',
                           -class    => 'newform',
                           -action   => $cgi->self_url,
                           -onsubmit => 'return validateRepoForm()',
                           -method   => 'POST'),
            # start table
            $cgi->start_table({-class => 'table'}),
              $cgi->Tr(
                $cgi->td('Родительский репозиторий'),
                $cgi->td(
                  $cgi->popup_menu(-id      => 'id_repo',
                                   -name    => 'id_repo',
                                   -values  => \@id_repo_values,
                                   -default => '',
                                   -labels  => \%id_repo_labels))),
              $cgi->Tr(
                $cgi->td('Название проекта'),
                $cgi->td(
                  $cgi->div({-class => 'input-append'},
                    $cgi->textfield({-id => 'id_name',
                                     -class => 'span2',
                                     -autofocus => 'autofocus',
                                     -type => 'text',
                                     -name => 'name',
                                     -maxlength => 255}),
                    $cgi->span({-class => 'add-on'}, '.git')))),
              $cgi->Tr(
                $cgi->td('Автор проекта'),
                $cgi->td(
                  $cgi->popup_menu(-id      => 'id_author',
                                   -values  => \@id_author_values,
                                   -default => '',
                                   -labels  => \%id_author_labels,
                                   -placeholder => 'Выберите автора...',
                                   -name    => 'author'))),
              $cgi->Tr(
                $cgi->td('Описание проекта'),
                $cgi->td(
                  $cgi->textarea({-id   => 'id_description',
                                  -rows => 3,
                                  -cols => 40,
                                  -name => 'description'}))),
            #end table
            $cgi->end_table,
            $cgi->submit({-name  => 'submit',
                          -class => 'btn btn-large',
                          -value => 'Создать репозиторий'}),
          #end form
          $cgi->end_form,
        # end div class='container'
        $cgi->end_div,
        # div class='footer'
        $cgi->div({-class => 'footer'}),
      #end div id='wrapper'
      $cgi->end_div,
      $cgi->end_html;

exit 0; # Exit from GET block

POST:
my $repo_main   = $cgi->param('id_repo');
my $repo_name   = translit(clearspace($cgi->param('name')));
my $repo_author = $cgi->param('author');
my $repo_desc   = $cgi->param('description');
my $repo = "$repo_main/$repo_name";
$repo = $repo_name if $repo_main eq '';

# Create directory and init repository
use File::Path qw(make_path);
use File::Finder;
my $directory = $repos_dir.'/'.$repo.'.git';
make_path($directory, {mode => 0775});
chdir($directory);
`git --bare init`;
chmod 0775, -1, File::Finder->in($directory);
`git --bare config gitweb.owner "$repo_author"`;
`git --bare config gitweb.URL "ssh://git\@git.bks.tv$directory"`;
`git --bare config gitweb.description "$repo_desc"`;
open my($fh), q{>}, $directory.'/'.'description' or die $!;
print $fh $repo_desc;
close $fh;
# End init repository

print $cgi->header(-charset    => 'utf-8'),
      $cgi->start_html( -title => 'Create repository',
                        -style => {'src'         => 'css/style.css'},
                        -head  => Link({-rel => 'shortcut icon', -href => '/kogakure/git-favicon.png'}),
                        -meta  => {'description' => 'Create git repository',
                                   'generator'   => "stl/$version",
                                   'robots'      => 'noindex,nofollow'}),
      # start div id='wrapper'
      $cgi->start_div({-id => 'wrapper'}),
        # div class='navbar'
        $cgi->div({-class => 'navbar'},
          # div id='navbar-inner'
          $cgi->div({-class => 'navbar-inner'},
            # start navigation menu
            # div class='brand'
            $cgi->div({-class  => 'brand'},
              $cgi->a({     -href   => '/',
                            -title  => 'BKS Git Server'},
                $cgi->img({ -src    => '/kogakure/git-logo.png',
                            -width  => 72,
                            -height => 27,
                            -alt    => 'git',
                            -class  => 'logo'}))),
            # end div class='brand'
            $cgi->ul({-class => 'nav'},
              $cgi->li($cgi->a({-href => '/'}, 'Home')),
              $cgi->li({-class => 'active'}, $cgi->a({-href => $cgi->self_url}, 'Create repository')),
              $cgi->li($cgi->a({-href => '/handler/edit.cgi'}, 'Edit repositories')),
              $cgi->li($cgi->a({-href => '/handler/users.cgi'}, 'Users'))))),
            # end navigation menu
        # end div class='navbar'
        # div class='container'
        $cgi->start_div({-class => 'container'}),
          $cgi->start_div({-class => 'text'}),
            $cgi->h2("Репозиторий $repo создан.");
            print $cgi->p("Автором указан: $repo_author.") if $repo_author ne '';
            print $cgi->p("Описание: $repo_desc") if $repo_desc ne '';
            print $cgi->p("Для дальнейшей работы с репозиторием выполните следующий список действий:"),
            $cgi->div({-class => 'well '},
              "cd project",
              $cgi->br,
              "git init",
              $cgi->br,
              "git config --global user.email \"you_email\@server.tld\"",
              $cgi->br,
              "git config --global user.name \"Your Name\"",
              $cgi->br,
              "git add .",
              $cgi->br,
              "git commit -m 'Initial commit'",
              $cgi->br,
              "git remote add origin git\@git.bks.tv:/projects/$repo.git",
              $cgi->br,
              "git push origin master"),
           $cgi->end_div,                    
        # end div class='content'
        $cgi->end_div,
        # div class='footer'
        $cgi->div({-class => 'footer'}),
      #end div id='wrapper'
      $cgi->end_div,
      $cgi->end_html;

exit 0; # Exit from POST block      

sub author_values
{
  my @authors = ('');
  my @files = <$keys_dir/*.pub>;
  foreach my $file (@files) {
    $file =~ s/$keys_dir\/(.*)\.pub/$1/;
    my $name = join ' ', map(ucfirst, split(/\./, $file));
    push @authors, $name;
  }
  return @authors;
}

sub author_labels
{
  my %labels;
  foreach my $value (@id_author_values) {$labels{$value} = $value}
  $labels{''} = '---------';
  return %labels;
}

sub repo_values
{
  my @values = ('');
  opendir my($dh), $repos_dir or die $!;
  my @listing = grep { !/^\.\.?$/ } readdir $dh;
  closedir $dh;
  foreach my $elem (@listing) {
    last unless -d "$repos_dir/$elem";
    push @values, $elem unless $elem =~ /^\.|\.git$/;
  }
  return @values;
}

sub repo_labels
{
  my %labels;
  foreach my $value (@id_repo_values) {$labels{$value} = $value}
  $labels{''} = '---------';
  return %labels;
}

sub clearspace
{
  ($_) = @_;
  s/ /_/;
  return $_;
}

sub translit
{
    ($_)=@_;

    s/Сх/S\'h/; s/сх/s\'h/; s/СХ/S\'H/;
    s/Ш/Sh/g; s/ш/sh/g;
    s/Сцх/Sc\'h/; s/сцх/sc\'h/; s/СЦХ/SC\'H/;
    s/Щ/Sch/g; s/щ/sch/g;
    s/Цх/C\'h/; s/цх/c\'h/; s/ЦХ/C\'H/;
    s/Ч/Ch/g; s/ч/ch/g;
    s/Йа/J\'a/; s/йа/j\'a/; s/ЙА/J\'A/;
    s/Я/Ja/g; s/я/ja/g;
    s/Йо/J\'o/; s/йо/j\'o/; s/ЙО/J\'O/;
    s/Ё/Jo/g; s/ё/jo/g;
    s/Йу/J\'u/; s/йу/j\'u/; s/ЙУ/J\'U/;
    s/Ю/Ju/g; s/ю/ju/g;
    s/Э/E\'/g; s/э/e\'/g;
    s/Е/E/g; s/е/e/g;
    s/Зх/Z\'h/g; s/зх/z\'h/g; s/ЗХ/Z\'H/g;
    s/Ж/Zh/g; s/ж/zh/g;
    tr/
    абвгдзийклмнопрстуфхцъыьАБВГДЗИЙКЛМНОПРСТУФХЦЪЫЬ/
    abvgdzijklmnoprstufhc\"y\'ABVGDZIJKLMNOPRSTUFHC\"Y\'/;
    
    return $_;
}
